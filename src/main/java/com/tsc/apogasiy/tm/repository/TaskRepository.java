package main.java.com.tsc.apogasiy.tm.repository;

import main.java.com.tsc.apogasiy.tm.api.repository.ITaskRepository;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task findById(String id) {
        for (Task task : tasks)
            if (id.equals(task.getId()))
                return task;
        return null;
    }

    @Override
    public Task findByName(String name) {
        for (Task task : tasks)
            if (name.equals(task.getName()))
                return task;
        return null;
    }

    @Override
    public Task findByIndex(int index) {
        return tasks.get(index);
    }

    @Override
    public Task removeById(String id) {
        final Task task = findById(id);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(String name) {
        final Task task = findByName(name);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(int index) {
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public boolean existsById(String id) {
        return findById(id) != null;
    }

    @Override
    public boolean existsByIndex(int index) {
        return index < tasks.size() && index >= 0;
    }

    @Override
    public boolean existsByName(String name) {
        return findByName(name) != null;
    }

    @Override
    public Task startById(String id) {
        final Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(Integer index) {
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(String name) {
        final Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(String id) {
        final Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(Integer index) {
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(String name) {
        final Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeStatusById(String id, Status status) {
        final Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(Integer index, Status status) {
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(String name, Status status) {
        final Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

}
